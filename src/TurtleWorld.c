/**
 * The Board class defines a board of 11 cells on the X axis by 5 cells 
 * on the Y axis, where each cell is drawn using a 60x60 size image 
 * built from the "sand.jpg" file. When a scenario is executed, Greenfoot 
 * executes the act() method of all the actors repeatedly, that is, the 
 * execution of a Greenfoot scenario is a sequence of cycles and in each 
 * cycle the act () method of all the acts it contains is executed scenario.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

#define _width    11
#define _height   5
#define _cellSize 60

/**
 * Initialize its background image.
 */
void startTurtleWorld(World turtleWorld) {
    setBackgroundFile(turtleWorld, "sand.jpg");
    
}

Actor getTurtle(World turtleWorld) {
    
}
