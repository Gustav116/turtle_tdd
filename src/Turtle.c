/**
 * The Turtle file defines a game actor that has a 50x40 size image built 
 * from the "Turtle.png" file. The act() method declared in the Turtle file 
 * defines the turtle behaviour in each cycle of the scenario execution.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "greenfoot.h"

/**
 * Initialize its image.
 */
void startTurtle(Actor turtle) {


}

void actTurtle(Actor turtle) {

}

void setAngle(Actor turtle, int angle) {
    
}


void setDistance(Actor turtle, int distance) {
   
}
