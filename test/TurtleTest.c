/**
 * Tests of Turtle. 
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "TurtleWorld.c"
#include "Turtle.c"




void testImage() {
    // Given
    Actor turtle = newActor("Turtle");

	// When
	const char* imageFile = getImageFile(turtle);

	// Then
	assertEquals_String("turtle.png", imageFile);
}

void testImageWidth() {
    // Given
    Actor turtle = newActor("Turtle");

    // When
    int width = getImageWidth(turtle);
        
    // Then
	assertEquals_int(50, width);
}

void testImageHeight() {
    // Given
    Actor turtle = newActor("Turtle");

    // When
    int height = getImageHeight(turtle);
        
    // Then
	assertEquals_int(40, height);
}

void testTurtleAct_X() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}

void testTurtleAct_Y() {
    // Given

    // When

    // Then
	fail("Not yet implemented");
}

void testSetAngle180() {
   
    // Given
  

    // When
  

    // Then

}
    
void testSetAngle270() {
    // Given
   
    
    // When
    
    // Then
	
}
    
void testSetDistance5Angle0() {
    
    // Given

    // When

    // Then

}

void testSetDistance3Angle90() {
    // Given

    // When

    // Then
	
}
