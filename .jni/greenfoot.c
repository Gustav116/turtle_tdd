/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _greenfoot_c
#define _greenfoot_c

#include <string.h>
#include "greenfoot.h"

extern JNIEnv *javaEnv;


void throwAssertionError(const char* message, const char* _file, const char* _function, int _line) {
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LTestResultPanel;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "setAssertInfo", "(Ljava/lang/String;Ljava/lang/String;I)V");
	(*javaEnv)->CallStaticIntMethod(javaEnv, cls, valueOf, toJstring(_file), toJstring(_function), toJint(_line));
	jclass assertExceptionClass = (*javaEnv)->FindClass(javaEnv, "java/lang/AssertionError");
    jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, assertExceptionClass, "<init>", "(Ljava/lang/String;)V");
    jthrowable j_exception = (jthrowable)(*javaEnv)->NewObject(javaEnv, assertExceptionClass, constructor, toJstring(message));
    (*javaEnv)->Throw(javaEnv, j_exception);
    fprintf(stderr,"Error at %s (%s:%d)\n", _function, _file, _line);
}

jclass toJclass_(const char* type) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	if (strcmp(type,"Actor") == 0) {
		return (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Actor;");

	} else if (strcmp(type,"World") == 0) {
		return (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/World;");

	} else if (strcmp(type,"Board") == 0) {
		return (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/TurnBased;");

	} else if (strcmp(type,"Piece") == 0) {
		return (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/TurnBased$Piece;");

	} else {
		char jniType[strlen(type)+3];
		strcpy(jniType, "L");
		strcat(jniType, type);
		strcat(jniType, ";");
		return (*javaEnv)->FindClass(javaEnv, jniType);
	}
}

const char* toType_(jobject object, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);
	jmethodID getClass = (*javaEnv)->GetMethodID(javaEnv, cls, "getClass", "()Ljava/lang/Class;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found type", _file, _function, _line);
    	return NULL;
    } else {
		jobject objectClass = (*javaEnv)->CallObjectMethod(javaEnv, object, getClass);
		cls = (*javaEnv)->GetObjectClass(javaEnv, objectClass);
		jmethodID getName = (*javaEnv)->GetMethodID(javaEnv, cls, "getName", "()Ljava/lang/String;");
		if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
			throwAssertionError("Not found type", _file, _function, _line);
	    	return NULL;
		} else {
			jstring j_name = (jstring)(*javaEnv)->CallObjectMethod(javaEnv, objectClass, getName);
		    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
		    	throwAssertionError("Not Found type", _file, _function, _line);
		    	return NULL;
		    } else {
		    	return toString(j_name);
		    }
		}
    }
}

jobject toJtype_(const char* type, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
    jclass cls = toJclass_(type);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found name", _file, _function, _line);
    	return NULL;
    } else {
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "()V");
		if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
			throwAssertionError("Not found name", _file, _function, _line);
			return NULL;
		} else {
			jobject j_type = (*javaEnv)->NewObject(javaEnv, cls, constructor);
			if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
				throwAssertionError("Not found name", _file, _function, _line);
				return NULL;
			} else {
				return j_type;
			}
		}
    }
}


//////////////////////////////////////////////////////
//           Global Variables operations            //
//////////////////////////////////////////////////////

void setGlobal_(jobject object, const char* name, jobject value, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, object, toJstring(name), value);
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
		throwAssertionError("Not set Global Variable", _file, _function, _line);
	}
}

jobject getGlobal_(jobject object, const char* name, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LGlobalVariable;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;");
	jobject j_object =(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod, object, toJstring(name));
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
		throwAssertionError("Not found Global Variable", _file, _function, _line);
		return NULL;
	} else {
		return j_object;
	}
}


//////////////////////////////////////////////////////
//                  Game operations                 //
//////////////////////////////////////////////////////

void setWorld_(World world, const char* _file, const char* _function, int _line) { //***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID setWorldMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "setWorld", "(Lgreenfoot/World;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setWorldMethod, world);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    }
}

const char* getKey_(const char* _file, const char* _function, int _line) { //***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID getKeyMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "getKey", "()Ljava/lang/String;");
    jstring j_getKey = (jstring)(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getKeyMethod);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return NULL;
    } else {
    	return toString(j_getKey);
    }
}

bool isKeyDown_(const char* key, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID isKeyDownMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "isKeyDown", "(Ljava/lang/String;)Z");
	jboolean j_isKeyDown = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, isKeyDownMethod, toJstring(key));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return false;
    } else {
    	return toBool(j_isKeyDown);
    }
}

void delay_(int time, const char* _file, const char* _function, int _line) { //***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID delayMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "delay", "(I)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, delayMethod, toJint(time));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    }
}

void setSpeed_(int speed, const char* _file, const char* _function, int _line) { //***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID setSpeedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "delay", "(I)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setSpeedMethod, toJint(speed));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    }
}

void stopScenario_(const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID stopMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "stop", "()V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, stopMethod);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    }
}

void startScenario_(const char* _file, const char* _function, int _line) { //***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID startMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "start", "()V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, startMethod);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    }
}

int getRandomNumber_(int limit, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID getRandomNumberMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "getRandomNumber", "(I)I");
	jint j_randomNumber = (*javaEnv)->CallStaticIntMethod(javaEnv, cls, getRandomNumberMethod, toJint(limit));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return 0;
    } else {
    	return toInt(j_randomNumber);
    }
}

void playSound_(const char* sound, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID playSoundMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "playSound", "(Ljava/lang/String;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, playSoundMethod, toJstring(sound));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    }
}

bool mousePressed_(Actor actor, const char* _file, const char* _function, int _line) {//***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID mousePressedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "mousePressed", "(Ljava/lang/Object;)Z");
	jboolean j_mousePressed = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, mousePressedMethod, actor);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return false;
    } else {
    	return toBool(j_mousePressed);
    }
}

bool mouseClicked_(Actor actor, const char* _file, const char* _function, int _line) {//***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID mouseClickedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "mouseClicked", "(Ljava/lang/Object;)Z");
	jboolean j_mouseClicked = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, mouseClickedMethod, actor);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return false;
    } else {
    	return toBool(j_mouseClicked);
    }
}

bool mouseDragged_(Actor actor, const char* _file, const char* _function, int _line) {//***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID mouseDraggedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "mouseDragged", "(Ljava/lang/Object;)Z");
	jboolean j_mouseDragged = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, mouseDraggedMethod, actor);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return false;
    } else {
    	return toBool(j_mouseDragged);
    }
}

bool mouseDragEnded_(Actor actor, const char* _file, const char* _function, int _line) {//***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID mouseDragEndedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "mouseDragEnded", "(Ljava/lang/Object;)Z");
	jboolean j_mouseDragEnded = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, mouseDragEndedMethod, actor);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return false;
    } else {
    	return toBool(j_mouseDragEnded);
    }
}

bool mouseMoved_(Actor actor, const char* _file, const char* _function, int _line) {//***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID mouseMovedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "mouseMoved", "(Ljava/lang/Object;)Z");
	jboolean j_mouseMoved = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, mouseMovedMethod, actor);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return false;
    } else {
    	return toBool(j_mouseMoved);
    }
}

int getMicLevel_(const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID getMicLevelMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "getMicLevel", "()I");
	jint j_micLevel = (*javaEnv)->CallStaticIntMethod(javaEnv, cls, getMicLevelMethod);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return 0;
    } else {
    	return toInt(j_micLevel);
    }
}

const char* ask_(const char* prompt, char* _file, const char* _function, int _line) { //***
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Greenfoot;");
    jmethodID askMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "ask", "(Ljava/lang/String;)Ljava/lang/String;");
    jstring j_ask = (jstring)(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, askMethod, toJstring(prompt));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return NULL;
    } else {
    	return toString(j_ask);
    }
}


//////////////////////////////////////////////////////
//                 Color operations                 //
//////////////////////////////////////////////////////

Color newColor_(int r, int g, int b, const char* name, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Color;");
    jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(IIILjava/lang/String;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    	return NULL;
    } else {
    	Color color = (*javaEnv)->NewObject(javaEnv, cls, constructor, toJint(r), toJint(g), toJint(b), toJstring(name));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return color;
	    }
    }
}

const char* toColorName_(Color color, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	if (color == NULL) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)color);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found color", _file, _function, _line);
    	return NULL;
    } else {
		jfieldID colorNameField = (*javaEnv)->GetFieldID(javaEnv, cls, "colorName", "Ljava/lang/String;");
		jstring j_colorName = (*javaEnv)->GetObjectField(javaEnv, color, colorNameField);
		if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
			throwAssertionError("Game error", _file, _function, _line);
			return NULL;
		} else {
			const char* colorName = toString(j_colorName);
			if (strstr(colorName, "Color.") == colorName) {
				colorName += 6;
			}
			return colorName;
		}
    }
//    jmethodID toStringMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "toString", "()Ljava/lang/String;");
//    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
//    	throwAssertionError("Not found color", _file, _function, _line);
//    	return NULL;
//    } else {
//    	jstring j_colorName = (jstring)(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, toStringMethod);
//	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
//	    	throwAssertionError("Game error", _file, _function, _line);
//	    	return NULL;
//	    } else {
//			return toString(j_colorName);
//	    }
//   }
}

Color toColor_(const char* colorName, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	if (colorName == NULL) return NULL;
    jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Color;");

    jfieldID colorsWithNameField = (*javaEnv)->GetStaticFieldID(javaEnv, cls, "colorsWithName", "Ljava/util/ArrayList;");
    jobject colorsWithNameObj = (*javaEnv)->GetStaticObjectField(javaEnv, cls, colorsWithNameField);

    jclass arrayListClass = (*javaEnv)->FindClass(javaEnv, "java/util/ArrayList");
    jmethodID getMethod = (*javaEnv)->GetMethodID(javaEnv, arrayListClass, "get", "(I)Ljava/lang/Object;");

    jfieldID colorNameField = (*javaEnv)->GetFieldID(javaEnv, cls, "colorName", "Ljava/lang/String;");

    jmethodID sizeMethod = (*javaEnv)->GetMethodID(javaEnv, arrayListClass, "size", "()I");
    jint size = (*javaEnv)->CallIntMethod(javaEnv, colorsWithNameObj, sizeMethod);

    if (strstr(colorName, "Color.") == colorName) {
        colorName += 6;
    }
    for (int i = 0; i < size; i++) {
        jobject j_color = (*javaEnv)->CallObjectMethod(javaEnv, colorsWithNameObj, getMethod, i);
        jstring j_colorNameField = (*javaEnv)->GetObjectField(javaEnv, j_color, colorNameField);
        const char* colorNameField = toString(j_colorNameField);
        if (strstr(colorNameField, "Color.") == colorNameField) {
        	colorNameField += 6;
        }
        if (strcasecmp(colorName, colorNameField) == 0) {
            if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
            	throwAssertionError("Game error", _file, _function, _line);
            	return NULL;
            } else {
            	return j_color;
            }
        }
    }
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Game error", _file, _function, _line);
    }
    return NULL;
}


//////////////////////////////////////////////////////
//                 World operations                 //
//////////////////////////////////////////////////////

World newWorld_(const char* type, const char* _file, const char* _function, int _line) {
	return (World)toJtype_(type, _file, _function, _line);
}

const char* getWorldType_(World world, const char* _file, const char* _function, int _line) {
	return toType_((jobject)world, _file, _function, _line);
}

int getWidth_(World world, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID addObjectMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getWidth", "()I");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    	return 0;
    } else {
		jint j_width = (*javaEnv)->CallIntMethod(javaEnv, (jobject)world, addObjectMethod, (jobject)world);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return 0;
	    } else {
	    	return toInt(j_width);
	    }
    }
}

int getHeight_(World world, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID addObjectMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getHeight", "()I");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    	return 0;
    } else {
		jint j_height = (*javaEnv)->CallIntMethod(javaEnv, (jobject)world, addObjectMethod, (jobject)world);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return 0;
	    } else {
	    	return toInt(j_height);
	    }
    }
}

int getCellSize_(World world, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID addObjectMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getCellSize", "()I");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    	return 0;
    } else {
		jint j_cellSize = (*javaEnv)->CallIntMethod(javaEnv, (jobject)world, addObjectMethod, (jobject)world);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return 0;
	    } else {
	    	return toInt(j_cellSize);
	    }
    }
}

const char* getBackgroundFile_(World world, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
	jmethodID getImage = (*javaEnv)->GetMethodID(javaEnv, cls, "getBackground", "()Lgreenfoot/GreenfootImage;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    	return NULL;
    } else {
		jobject j_image = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)world, getImage);
		cls = (*javaEnv)->GetObjectClass(javaEnv, j_image);
		jmethodID getImageName = (*javaEnv)->GetMethodID(javaEnv, cls, "getImageFileName", "()Ljava/lang/String;");
		jstring j_imageName = (jstring)(*javaEnv)->CallObjectMethod(javaEnv, j_image, getImageName);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return toString(j_imageName);
	    }
    }
}

void setBackgroundFile_(World world, const char* imageFileName, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID setImage = (*javaEnv)->GetMethodID(javaEnv, cls, "setBackground", "(Ljava/lang/String;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)world, setImage, toJstring(imageFileName));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void addActorToWorld_(World world, Actor actor, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID addObjectMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "addObject", "(Lgreenfoot/Actor;II)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)world, addObjectMethod, (jobject)actor, toJint(x), toJint(y));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

Actor* getActors_(World world, const char* actorType, int* actorsNumber, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID getObjects = (*javaEnv)->GetMethodID(javaEnv, cls, "getObjects", "(Ljava/lang/Class;)Ljava/util/List;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    	return NULL;
    } else {
        jclass actorClass = toJclass_(actorType);
        if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
        	throwAssertionError("Not found name", _file, _function, _line);
        	return NULL;
        } else {
			jobject j_actors = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)world, getObjects, actorClass);
			jclass cls_List = (*javaEnv)->FindClass(javaEnv, "Ljava/util/List;");
			jmethodID sizeMethod = (*javaEnv)->GetMethodID(javaEnv, cls_List, "size", "()I");
			jint j_actorsNumber = (*javaEnv)->CallIntMethod(javaEnv, j_actors, sizeMethod);
			*actorsNumber = toInt(j_actorsNumber);
			jmethodID getMethod = (*javaEnv)->GetMethodID(javaEnv, cls_List, "get", "(I)Ljava/lang/Object;");
			Actor* actors = (Actor*) malloc(*actorsNumber * sizeof(Actor));
			for (int k = 0; k < *actorsNumber; k++) {
				actors[k] = (Actor)(*javaEnv)->CallObjectMethod(javaEnv, j_actors, getMethod, toJint(k));
			}
			if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
				throwAssertionError("Game error", _file, _function, _line);
				return NULL;
			} else {
				return actors;
			}
        }
    }
}

int getActorsNumber_(World world, const char* actorType, const char* _file, const char* _function, int _line){
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID getObjects = (*javaEnv)->GetMethodID(javaEnv, cls, "getObjects", "(Ljava/lang/Class;)Ljava/util/List;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    	return 0;
    } else {
        jclass actorClass = toJclass_(actorType);
        if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
        	throwAssertionError("Not found name", _file, _function, _line);
        	return 0;
        } else {
			jobject j_actors = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)world, getObjects, actorClass);
			jclass cls_List = (*javaEnv)->FindClass(javaEnv, "Ljava/util/List;");
			jmethodID sizeMethod = (*javaEnv)->GetMethodID(javaEnv, cls_List, "size", "()I");
			jint j_actorsNumber = (*javaEnv)->CallIntMethod(javaEnv, j_actors, sizeMethod);
			if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
				throwAssertionError("Game error", _file, _function, _line);
				return 0;
			} else {
				return toInt(j_actorsNumber);
			}
        }
    }
}

Actor* getActorsAt_(World world, int x, int y, const char* actorType, int* actorsNumber, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID getObjects = (*javaEnv)->GetMethodID(javaEnv, cls, "getObjectsAt", "(IILjava/lang/Class;)Ljava/util/List;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    	return NULL;
    } else {
        jclass actorClass = toJclass_(actorType);
        if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
        	throwAssertionError("Not found name", _file, _function, _line);
        	return NULL;
        } else {
			jint j_x = toJint(x);
			jint j_y = toJint(y);
			jobject j_actors = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)world, getObjects, j_x, j_y, actorClass);
			jclass cls_List = (*javaEnv)->FindClass(javaEnv, "Ljava/util/List;");
			jmethodID sizeMethod = (*javaEnv)->GetMethodID(javaEnv, cls_List, "size", "()I");
			jint j_actorsNumber = (*javaEnv)->CallIntMethod(javaEnv, j_actors, sizeMethod);
			*actorsNumber = toInt(j_actorsNumber);
			jmethodID getMethod = (*javaEnv)->GetMethodID(javaEnv, cls_List, "get", "(I)Ljava/lang/Object;");
			Actor* actors = (Actor*) malloc(*actorsNumber * sizeof(Actor));
			for (int k = 0; k < *actorsNumber; k++) {
				actors[k] = (Actor)(*javaEnv)->CallObjectMethod(javaEnv, j_actors, getMethod, toJint(k));
			}
			if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
				throwAssertionError("Game error", _file, _function, _line);
				return NULL;
			} else {
				return actors;
			}
        }
    }
}

int getActorsNumberAt_(World world, int x, int y, const char* actorType, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID getObjects = (*javaEnv)->GetMethodID(javaEnv, cls, "getObjectsAt", "(IILjava/lang/Class;)Ljava/util/List;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    	return 0;
    } else {
        jclass actorClass = toJclass_(actorType);
        if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
        	throwAssertionError("Not found name", _file, _function, _line);
        	return 0;
        } else {
			jint j_x = toJint(x);
			jint j_y = toJint(y);
			jobject j_actors = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)world, getObjects, j_x, j_y, actorClass);
			jclass cls_List = (*javaEnv)->FindClass(javaEnv, "Ljava/util/List;");
			jmethodID sizeMethod = (*javaEnv)->GetMethodID(javaEnv, cls_List, "size", "()I");
			jint j_actorsNumber = (*javaEnv)->CallIntMethod(javaEnv, j_actors, sizeMethod);
			if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
				throwAssertionError("Game error", _file, _function, _line);
				return 0;
			} else {
				return toInt(j_actorsNumber);
			}
        }
    }
}

Actor* getActOrderActors_(World world, int* actOrderActorsNumber, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID getActOrderActorsMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getActOrderActors", "()Ljava/util/List;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    	return NULL;
    } else {
		jobject j_actors = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)world, getActOrderActorsMethod);
		jclass cls_List = (*javaEnv)->FindClass(javaEnv, "Ljava/util/List;");
		jmethodID sizeMethod = (*javaEnv)->GetMethodID(javaEnv, cls_List, "size", "()I");
		jint j_actOrderActorsNumber = (*javaEnv)->CallIntMethod(javaEnv, j_actors, sizeMethod);
		*actOrderActorsNumber = toInt(j_actOrderActorsNumber);
		jmethodID getMethod = (*javaEnv)->GetMethodID(javaEnv, cls_List, "get", "(I)Ljava/lang/Object;");
		Actor* actors = (Actor*) malloc(*actOrderActorsNumber * sizeof(Actor));
		for (int k = 0; k < *actOrderActorsNumber; k++) {
			actors[k] = (Actor)(*javaEnv)->CallObjectMethod(javaEnv, j_actors, getMethod, toJint(k));
		}
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return actors;
	    }
    }
}

void setActOrderActors_(World world, Actor* actors, int actorsNumber, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID setActOrderActorsMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setActOrder", "([Lgreenfoot/Actor;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    } else {
    	jobjectArray actorsArray = (*javaEnv)->NewObjectArray(javaEnv, toJint(actorsNumber), (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Actor;"), NULL);
    	for (int k=0; k < actorsNumber; k++) {
    		(*javaEnv)->SetObjectArrayElement(javaEnv, actorsArray, k, actors[k]);
    	}
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)world, setActOrderActorsMethod, actorsArray);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void setPaintOrder_(World world, Actor* actors, int actorsNumber, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID setPaintOrderMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setPaintOrder", "([Lgreenfoot/Actor;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    } else {
    	jobjectArray actorsArray = (*javaEnv)->NewObjectArray(javaEnv, toJint(actorsNumber), (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Actor;"), NULL);
    	for (int k=0; k < actorsNumber; k++) {
    		(*javaEnv)->SetObjectArrayElement(javaEnv, actorsArray, k, actors[k]);
    	}
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)world, setPaintOrderMethod, actorsArray);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void removeActor_(World world, Actor actor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID removeActorMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "removeObject", "(Lgreenfoot/Actor;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)world, removeActorMethod, actor);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}
void removeActors_(World world, Actor* actors, int actorsNumber, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID removeActorsMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "removeObjects", "([Lgreenfoot/Actor;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    } else {
    	jobjectArray actorsArray = (*javaEnv)->NewObjectArray(javaEnv, toJint(actorsNumber), (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Actor;"), NULL);
    	for (int k=0; k < actorsNumber; k++) {
    		(*javaEnv)->SetObjectArrayElement(javaEnv, actorsArray, k, actors[k]);
    	}
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)world, removeActorsMethod, actorsArray);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

Color getColorAt_(World world, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID getColorAtMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getColorAt", "(II)greenfoot/Color;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    	return NULL;
    } else {
		jint j_x = toJint(x);
		jint j_y = toJint(y);
		jobject j_color = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)world, getColorAtMethod, j_x, j_y);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return (Color)j_color;
	    }
    }
}

void repaint_(World world, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID repaintMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "repaint", "()V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)world, repaintMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void showText_(World world, const char* text, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)world);
    jmethodID showTextMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "repaint", "()V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found world", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)world, showTextMethod, toJstring(text), toJint(x), toJint(y));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}


//////////////////////////////////////////////////////
//                 Actor operations                 //
//////////////////////////////////////////////////////

Actor newActor_(const char* type, const char* _file, const char* _function, int _line) {
	return (Actor)toJtype_(type, _file, _function, _line);
}

const char* getActorType_(Actor actor, const char* _file, const char* _function, int _line) {
	return toType_((jobject)actor, _file, _function, _line);
}

void setImageFile_(Actor actor, const char* imageFileName, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID setImage = (*javaEnv)->GetMethodID(javaEnv, cls, "setImage", "(Ljava/lang/String;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)actor, setImage, toJstring(imageFileName));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Image not found", _file, _function, _line);
	    }
    }
}

const char* getImageFile_(Actor actor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
	jmethodID getImage = (*javaEnv)->GetMethodID(javaEnv, cls, "getImage", "()Lgreenfoot/GreenfootImage;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    	return NULL;
    } else {
		jobject j_image = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)actor, getImage);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Image not found", _file, _function, _line);
	    	return NULL;
	    } else {
			cls = (*javaEnv)->GetObjectClass(javaEnv, j_image);
			jmethodID getImageName = (*javaEnv)->GetMethodID(javaEnv, cls, "getImageFileName", "()Ljava/lang/String;");
			jstring j_imageName = (jstring)(*javaEnv)->CallObjectMethod(javaEnv, j_image, getImageName);
		    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
		    	throwAssertionError("Game error", _file, _function, _line);
		    	return NULL;
		    } else {
		    	return toString(j_imageName);
		    }
	    }
    }
}

int getImageWidth_(Actor actor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
	jmethodID getImage = (*javaEnv)->GetMethodID(javaEnv, cls, "getImage", "()Lgreenfoot/GreenfootImage;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    	return 0;
    } else {
		jobject j_image = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)actor, getImage);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Image not found", _file, _function, _line);
	    	return 0;
	    } else {
			cls = (*javaEnv)->GetObjectClass(javaEnv, j_image);
			jmethodID getImageWidthMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getWidth", "()I");
			jint j_width = (*javaEnv)->CallIntMethod(javaEnv, j_image, getImageWidthMethod);
		    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
		    	throwAssertionError("Game error", _file, _function, _line);
		    	return 0;
		    } else {
		    	return toInt(j_width);
		    }
	    }
    }
}

int getImageHeight_(Actor actor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
	jmethodID getImage = (*javaEnv)->GetMethodID(javaEnv, cls, "getImage", "()Lgreenfoot/GreenfootImage;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    	return 0;
    } else {
		jobject j_image = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)actor, getImage);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Image not found", _file, _function, _line);
	    	return 0;
	    } else {
			cls = (*javaEnv)->GetObjectClass(javaEnv, j_image);
			jmethodID getImageHeightMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getHeight", "()I");
			jint j_height = (*javaEnv)->CallIntMethod(javaEnv, j_image, getImageHeightMethod);
		    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
		    	throwAssertionError("Game error", _file, _function, _line);
		    	return 0;
		    } else {
		    	return toInt(j_height);
		    }
	    }
    }
}

void setImageScale_(Actor actor, int width, int height, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
	jmethodID getImage = (*javaEnv)->GetMethodID(javaEnv, cls, "getImage", "()Lgreenfoot/GreenfootImage;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    } else {
		jobject j_image = (*javaEnv)->CallObjectMethod(javaEnv, (jobject)actor, getImage);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Image not found", _file, _function, _line);
	    } else {
			cls = (*javaEnv)->GetObjectClass(javaEnv, j_image);
			jmethodID scaleMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "scale", "(II)V");
			(*javaEnv)->CallVoidMethod(javaEnv, j_image, scaleMethod, toJint(width), toJint(height));
		    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
		    	throwAssertionError("Game error", _file, _function, _line);
		    }
	    }
    }
}

World getWorld_(Actor actor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID getWorldMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getWorld", "()Lgreenfoot/World;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    	return NULL;
    } else {
    	World world = (World)(*javaEnv)->CallObjectMethod(javaEnv, (jobject)actor, getWorldMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Actor not in world", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return world;
	    }
    }
}

int getX_(Actor actor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID getXMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getX", "()I");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    	return 0;
    } else {
		jint j_x = (*javaEnv)->CallIntMethod(javaEnv, (jobject)actor, getXMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Actor not in world", _file, _function, _line);
	    	return 0;
	    } else {
	    	return toInt(j_x);
	    }
    }
}

int getY_(Actor actor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID getYMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getY", "()I");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    	return 0;
    } else {
		jint j_y = (*javaEnv)->CallIntMethod(javaEnv, (jobject)actor, getYMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Actor not in world", _file, _function, _line);
	    	return 0;
	    } else {
	    	return toInt(j_y);
	    }
    }
}

int getRotation_(Actor actor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID getRotationMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getRotation", "()I");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    	return 0;
    } else {
		jint j_rotation = (*javaEnv)->CallIntMethod(javaEnv, (jobject)actor, getRotationMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Actor not in world", _file, _function, _line);
	    	return 0;
	    } else {
	    	return toInt(j_rotation);
	    }
    }
}
void setLocation_(Actor actor, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID setLocationMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setLocation", "(II)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    } else {
        jint j_x = toJint(x);
        jint j_y = toJint(y);
        (*javaEnv)->CallVoidMethod(javaEnv, (jobject)actor, setLocationMethod, j_x, j_y);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Actor not in world", _file, _function, _line);
	    }
    }
}

void setRotation_(Actor actor, int angle, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID setRotationMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setRotation", "(I)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    } else {
		jint j_angle = toJint(angle);
		(*javaEnv)->CallVoidMethod(javaEnv, (jobject)actor, setRotationMethod, j_angle);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Actor not in world", _file, _function, _line);
	    }
    }
}

void move_(Actor actor, int distance, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID moveMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "move", "(I)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)actor, moveMethod, toJint(distance));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Actor not in world", _file, _function, _line);
	    }
    }
}

void turn_(Actor actor, int angle, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID turnMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "turn", "(I)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)actor, turnMethod, toJint(angle));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Actor not in world", _file, _function, _line);
	    }
    }
}

void turnTorward_(Actor actor, int angle, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID turnMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "turnTorward", "(I)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, (jobject)actor, turnMethod, toJint(angle));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Actor not in world", _file, _function, _line);
	    }
    }
}

bool isTouching_(Actor actor, const char* actorType, const char* _file, const char* _function, int _line){
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID isTouchingMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "isTouching", "(Ljava/lang/Class;)Z");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    	return false;
    } else {
        jclass actorClass = toJclass_(actorType);
        if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
        	throwAssertionError("Not found name", _file, _function, _line);
        	return NULL;
        } else {
			jboolean j_isTouching = (*javaEnv)->CallBooleanMethod(javaEnv, (jobject)actor, isTouchingMethod, actorClass);
			if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
				throwAssertionError("Actor not in world", _file, _function, _line);
				return false;
			} else {
				return toBool(j_isTouching);
			}
        }
    }
}

void removeTouching_(Actor actor, const char* actorType, const char* _file, const char* _function, int _line){
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID removeTouchingMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "removeTouching", "(Ljava/lang/Class;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    } else {
        jclass actorClass = toJclass_(actorType);
        if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
        	throwAssertionError("Not found name", _file, _function, _line);
        } else {
			(*javaEnv)->CallVoidMethod(javaEnv, (jobject)actor, removeTouchingMethod, actorClass);
			if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
				throwAssertionError("Actor not in world", _file, _function, _line);
			}
        }
    }
}

bool isAtEdge_(Actor actor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, (jobject)actor);
    jmethodID isAtEdgeMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "isAtEdge", "()Z");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found actor", _file, _function, _line);
    	return false;
    } else {
		jboolean j_isAtEdge = (*javaEnv)->CallBooleanMethod(javaEnv, (jobject)actor, isAtEdgeMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Actor not in world", _file, _function, _line);
	    	return false;
	    } else {
	    	return toBool(j_isAtEdge);
	    }
    }
}


//////////////////////////////////////////////////////
//                Board operations                  //
//////////////////////////////////////////////////////

Board newBoard_(const char* type, const char* _file, const char* _function, int _line) {
	return (Board)toJtype_(type, _file, _function, _line);
}

const char* getBoardType_(Board board, const char* _file, const char* _function, int _line) {
	return toType_((jobject)board, _file, _function, _line);
}

void setCellColor_(Board board, int x, int y, const char* color, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setCellColorMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setCellColor", "(IILgreenfoot/Color;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setCellColorMethod, toJint(x), toJint(y), toColor_(color, _file, _function, _line));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void setCellColorWithBorder_(Board board, int x, int y, const char* borderColor, const char* insideColor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setCellColorMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setCellColor", "(IILgreenfoot/Color;Lgreenfoot/Color;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setCellColorMethod, toJint(x), toJint(y), toColor_(borderColor, _file, _function, _line), toColor_(insideColor, _file, _function, _line));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

const char* getCellBorderColor_(Board board, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getCellBorderColorMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getCellBorderColor", "(II)Lgreenfoot/Color;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return NULL;
    } else {
    	jobject j_cellBorderColor = (*javaEnv)->CallObjectMethod(javaEnv, board, getCellBorderColorMethod, toJint(x), toJint(y));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return toColorName(j_cellBorderColor);
	    }
    }
}

const char* getCellInsideColor_(Board board, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getCellInsideColorMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getCellInsideColor", "(II)Lgreenfoot/Color;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return NULL;
    } else {
    	jobject j_cellInsideColor = (*javaEnv)->CallObjectMethod(javaEnv, board, getCellInsideColorMethod, toJint(x), toJint(y));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return toColorName(j_cellInsideColor);
	    }
    }
}

void setBackgroundColor_(Board board, const char*  color, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setBackgroundMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setBackground", "(Ljava/lang/String;Ljava/lang/String;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setBackgroundMethod, toJstring(color), toJstring(color));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void setBackgroundColorWithBorder_(Board board, const char*  borderColor, const char*  insideColor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setBackgroundMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setBackground", "(Ljava/lang/String;Ljava/lang/String;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setBackgroundMethod, toJstring(borderColor), toJstring(insideColor));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void setBoardBackground_(Board board, const char** cellColor, int rows, int columns, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setBackgroundMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setBackground", "([[Ljava/lang/String;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setBackgroundMethod, toJstringMatrixRegion(cellColor, &rows, &columns));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void setBoardBackgroundWithBorder_(Board board, const char* borderColor, const char** cellColor, int rows, int columns, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setBackgroundMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setBackground", "(Lgreenfoot/Color;[[Ljava/lang/String;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setBackgroundMethod, toColor_(borderColor, _file, _function, _line), toJstringMatrixRegion(cellColor, &rows, &columns));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

const char** getBoardBackground_(Board board, const char** colors, int colorsLength, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getBackgroundMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getBackgroundColorName", "([Ljava/lang/String;)[[Ljava/lang/String;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return NULL;
    } else {
    	jobjectArray j_boardBackground =(*javaEnv)->CallObjectMethod(javaEnv, board, getBackgroundMethod, toJstringArray(colors, &colorsLength));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return toStringMatrixRegion(j_boardBackground, getWidth(board), getHeight(board));
	    }
    }
}

void setTurnColor_(Board board, const char* turnColor, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setTurnColorMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setTurnColor", "(Lgreenfoot/Color;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setTurnColorMethod, toColor_(turnColor, _file, _function, _line));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

const char* getTurnColor_(Board board, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getTurnColorMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getTurnColor", "()Lgreenfoot/Color;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return NULL;
    } else {
    	jobject j_turnColor = (*javaEnv)->CallObjectMethod(javaEnv, board, getTurnColorMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return toColorName(j_turnColor);
	    }
    }
}

bool thereIsSelectedPiece_(Board board, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID thereIsSelectedPieceMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "thereIsSelectedPiece", "()Z");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return false;
    } else {
    	jboolean j_thereIsSelectedPiece = (*javaEnv)->CallBooleanMethod(javaEnv, board, thereIsSelectedPieceMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return false;
	    } else {
	    	return toBool(j_thereIsSelectedPiece);
	    }
    }
}

Piece getSelectedPiece_(Board board, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getSelectedPieceMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getSelectedPiece", "()Lgreenfoot/TurnBased$Piece;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return NULL;
    } else {
    	jobject j_selectedPiece = (*javaEnv)->CallObjectMethod(javaEnv, board, getSelectedPieceMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return j_selectedPiece;
	    }
    }
}

bool thereIsPartialMovement_(Board board, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID thereIsPartialMovementMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "thereIsPartialMovement", "()Z");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return false;
    } else {
    	jboolean j_thereIsSelectedPiece = (*javaEnv)->CallBooleanMethod(javaEnv, board, thereIsPartialMovementMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return false;
	    } else {
	    	return toBool(j_thereIsSelectedPiece);
	    }
    }
}

Piece getMovedPiece_(Board board, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getMovedPieceMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getMovedPiece", "()Lgreenfoot/TurnBased$Piece;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return NULL;
    } else {
    	jobject j_selectedPiece = (*javaEnv)->CallObjectMethod(javaEnv, board, getMovedPieceMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return j_selectedPiece;
	    }
    }
}

int getMovedPieceX_(Board board, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getMovedPieceXMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getMovedPieceX", "()I");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return 0;
    } else {
    	jint j_movedPieceX = (*javaEnv)->CallIntMethod(javaEnv, board, getMovedPieceXMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return 0;
	    } else {
	    	return toInt(j_movedPieceX);
	    }
    }
}

int getMovedPieceY_(Board board, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getMovedPieceYMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getMovedPieceY", "()I");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return 0;
    } else {
    	jint j_movedPieceY = (*javaEnv)->CallIntMethod(javaEnv, board, getMovedPieceYMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return 0;
	    } else {
	    	return toInt(j_movedPieceY);
	    }
    }
}

Piece getPiece_(Board board, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getPieceMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getPiece", "(II)Lgreenfoot/TurnBased$Piece;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return NULL;
    } else {
    	jobject j_piece = (*javaEnv)->CallObjectMethod(javaEnv, board, getPieceMethod, toJint(x), toJint(y));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return j_piece;
	    }
    }
}

int piecesNumber_(Board board, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID pieceNumberMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getPiecesNumber", "()I");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return 0;
    } else {
    	jint j_pieceNumber = (*javaEnv)->CallIntMethod(javaEnv, board, pieceNumberMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return 0;
	    } else {
	    	return toInt(j_pieceNumber);
	    }
    }
}

int piecesNumberAt_(Board board, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID pieceNumberAtMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "piecesNumber", "(II)I");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return 0;
    } else {
    	jint j_pieceNumberAt = (*javaEnv)->CallIntMethod(javaEnv, board, pieceNumberAtMethod, toJint(x), toJint(y));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return 0;
	    } else {
	    	return toInt(j_pieceNumberAt);
	    }
    }
}


bool thereIsPieceAt_(Board board, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID thereIsPieceMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "thereIsPiece", "(II)Z");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return false;
    } else {
    	jboolean j_thereIsPiece = (*javaEnv)->CallBooleanMethod(javaEnv, board, thereIsPieceMethod, toInt(x), toInt(y));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return false;
	    } else {
	    	return toBool(j_thereIsPiece);
	    }
    }
}

bool thereIsColorPieceAt_(Board board, int x, int y, const char* color, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID thereIsColorPieceMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "thereIsPiece", "(IILgreenfoot/Color;)Z");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return false;
    } else {
    	jboolean j_thereIsColorPiece = (*javaEnv)->CallBooleanMethod(javaEnv, board, thereIsColorPieceMethod, toInt(x), toInt(y), toColor_(color, _file, _function, _line));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return false;
	    } else {
	    	return toBool(j_thereIsColorPiece);
	    }
    }
}

bool canBeSelected_(Board board, Piece piece, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID canBeSelectedMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "canBeSelected", "(Lgreenfoot/TurnBased$Piece;)Z");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return false;
    } else {
    	jboolean j_canBeSelected = (*javaEnv)->CallBooleanMethod(javaEnv, board, canBeSelectedMethod, piece);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return false;
	    } else {
	    	return toBool(j_canBeSelected);
	    }
    }
}

void setSelectedPiece_(Board board, Piece piece, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setSelectedPieceMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setSelectedPiece", "(Lgreenfoot/TurnBased$Piece;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setSelectedPieceMethod, piece);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void setNullSelectedPiece_(Board board, const char* color, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setNullSelectedPieceMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setNullSelectedPiece", "(Lgreenfoot/TurnBased$Piece;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setNullSelectedPieceMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void setBoardPieces_(Board board, const char** pieces, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setBackgroundMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setPieces", "([[Ljava/lang/String;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	int rows = getWidth(board);
    	int columns = getHeight(board);
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setBackgroundMethod, toJstringMatrixRegion(pieces, &rows, &columns));
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}


const char** getBoardPieces_(Board board, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getPiecesMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getPieces", "()[[Ljava/lang/String;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return NULL;
    } else {
    	jobjectArray j_pieces =(*javaEnv)->CallObjectMethod(javaEnv, board, getPiecesMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return toStringMatrixRegion(j_pieces, getWidth(board), getHeight(board));
	    }
    }
}

const char** getBoardSquares_(Board board, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID getSquaresMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "getSquares", "()[[Ljava/lang/String;");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    	return NULL;
    } else {
    	jobjectArray j_squares =(*javaEnv)->CallObjectMethod(javaEnv, board, getSquaresMethod);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    	return NULL;
	    } else {
	    	return toStringMatrixRegion(j_squares, getWidth(board), getHeight(board));
	    }
    }
}

void setGreenSquareAt_(Board board, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setGreenSquareAtMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setGreenSquareAt", "(II[Lgreenfoot/TurnBased$Piece;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
     	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	jobjectArray piecesArray = (*javaEnv)->NewObjectArray(javaEnv, toJint(0), (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/TurnBased$Piece;"), NULL);
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setGreenSquareAtMethod, toJint(x), toJint(y), piecesArray);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void setGreenSquareWithEatenPiecesAt_(Board board, int x, int y, Piece* eatenPieces, int eatenPiecesNumber, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
    jmethodID setGreenSquareAtMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setGreenSquareAt", "(II[Lgreenfoot/TurnBased$Piece;)V");
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Not found board", _file, _function, _line);
    } else {
    	jobjectArray piecesArray = (*javaEnv)->NewObjectArray(javaEnv, toJint(eatenPiecesNumber), (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/TurnBased$Piece;"), NULL);
    	for (int k=0; k < eatenPiecesNumber; k++) {
    		(*javaEnv)->SetObjectArrayElement(javaEnv, piecesArray, k, eatenPieces[k]);
    	}
    	(*javaEnv)->CallVoidMethod(javaEnv, board, setGreenSquareAtMethod, toJint(x), toJint(y), piecesArray);
	    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
	    	throwAssertionError("Game error", _file, _function, _line);
	    }
    }
}

void setYellowSquareAt_(Board board, int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
	jmethodID setYellowSquareAtMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setYellowSquareAt", "(II)V");
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
		throwAssertionError("Not found board", _file, _function, _line);
	} else {
		(*javaEnv)->CallVoidMethod(javaEnv, board, setYellowSquareAtMethod, toJint(x), toJint(y));
		if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
			throwAssertionError("Game error", _file, _function, _line);
		}
	}
}

void setYellowSquareWithNextPieceAt_(Board board, int x, int y, Piece nextPiece, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->GetObjectClass(javaEnv, board);
	jmethodID setYellowSquareAtMethod = (*javaEnv)->GetMethodID(javaEnv, cls, "setYellowSquareAt", "(IILgreenfoot/TurnBased$Piece;)V");
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
		throwAssertionError("Not found piece", _file, _function, _line);
	} else {
		(*javaEnv)->CallVoidMethod(javaEnv, board, setYellowSquareAtMethod, toJint(x), toJint(y), nextPiece);
		if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
			throwAssertionError("Game error", _file, _function, _line);
		}
	}
}


//////////////////////////////////////////////////////
//                 Piece operations                 //
//////////////////////////////////////////////////////

Piece newPiece_(const char* type, const char* _file, const char* _function, int _line) {
	return (Piece)toJtype_(type, _file, _function, _line);
}

const char* getPieceType_(Piece piece, const char* _file, const char* _function, int _line) {
	return toType_((jobject)piece, _file, _function, _line);
}


//////////////////////////////////////////////////////
//       Simulation operations for Testing          //
//////////////////////////////////////////////////////

void runOnce_(const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/WorldCreator;");
    jmethodID runOnceMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "runOnce", "()V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, runOnceMethod);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void runOnceWorld_(World world, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/WorldCreator;");
    jmethodID runOnceMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "runOnce", "(Lgreenfoot/World;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, runOnceMethod, (jobject)world);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void runOnceActors_(Actor* actors, int actorsLength, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/WorldCreator;");
    jmethodID runOnceMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "runOnce", "([Lgreenfoot/Actor;)V");
	jobjectArray actorsArray = (*javaEnv)->NewObjectArray(javaEnv, toJint(actorsLength), (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Actor;"), NULL);
	for (int k=0; k < actorsLength; k++) {
		(*javaEnv)->SetObjectArrayElement(javaEnv, actorsArray, k, actors[k]);
	}
    (*javaEnv)->CallStaticVoidMethod(javaEnv, cls, runOnceMethod, actorsArray);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void runOnceWorldWithdActors_(World world, Actor* actors, int actorsLength, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/WorldCreator;");
    jmethodID runOnceMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "runOnce", "(Lgreenfoot/World;[Lgreenfoot/Actor;)V");
	jobjectArray actorsArray = (*javaEnv)->NewObjectArray(javaEnv, toJint(actorsLength), (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/Actor;"), NULL);
	for (int k=0; k < actorsLength; k++) {
		(*javaEnv)->SetObjectArrayElement(javaEnv, actorsArray, k, actors[k]);
	}
    (*javaEnv)->CallStaticVoidMethod(javaEnv, cls, runOnceMethod, world, actorsArray);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

bool isWorldStarted_(const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/WorldCreator;");
    jmethodID isStopMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "isStart", "()Z");
	jboolean j_isWorldStarted = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, isStopMethod);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    	return false;
    } else {
    	return toBool(j_isWorldStarted);
    }
}

bool isWorldStopped_(const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/WorldCreator;");
    jmethodID isStopMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "isStop", "()Z");
	jboolean j_isWorldStopped = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, isStopMethod);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    	return false;
    } else {
    	return toBool(j_isWorldStopped);
    }
}

bool isSpeed_(int speed, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/WorldCreator;");
    jmethodID isSpeedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "isSpeed", "(I)Z");
	jboolean j_isSpeed = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, isSpeedMethod, toJint(speed));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    	return false;
    } else {
    	return toBool(j_isSpeed);
    }
}

int getSpeed_(const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/WorldCreator;");
    jmethodID isSpeedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "getSpeed", "()I");
	jint j_speed = (*javaEnv)->CallStaticIntMethod(javaEnv, cls, isSpeedMethod);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    	return 0;
    } else {
    	return toInt(j_speed);
    }
}

bool isDelay_(int delay, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return false;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/WorldCreator;");
    jmethodID isSpeedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "isDelay", "(I)Z");
	jboolean j_isDelay = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, isSpeedMethod, toJint(delay));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    	return false;
    } else {
    	return toBool(j_isDelay);
    }
}

int getDelay_(const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return 0;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/WorldCreator;");
    jmethodID isSpeedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "getDelay", "()I");
	jint j_delay = (*javaEnv)->CallStaticIntMethod(javaEnv, cls, isSpeedMethod);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    	return 0;
    } else {
    	return toInt(j_delay);
    }
}

const char* getPlayedSound_(const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/SoundPlayed;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "()Ljava/lang/String;");
	jstring j_playedSound = (jstring)(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, getMethod);
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    	return NULL;
    } else {
    	return toString(j_playedSound);
    }
}

bool isPlayed_(const char* sound, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return NULL;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/SoundPlayed;");
    jmethodID getMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "get", "(Ljava/lang/String;)Z");
	jboolean j_isPlayedSound = (*javaEnv)->CallStaticBooleanMethod(javaEnv, cls, getMethod, toJstring(sound));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    	return NULL;
    } else {
    	return toBool(j_isPlayedSound);
    }
}

void typeKey_(const char* key, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/EventDispatch;");
    jmethodID keyTypedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "keyTyped", "(Ljava/lang/String;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, keyTypedMethod, toJstring(key));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void pressKey_(const char* key, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/EventDispatch;");
    jmethodID keyTypedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "keyPressed", "(Ljava/lang/String;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, keyTypedMethod, toJstring(key));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void releaseKey_(const char* key, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/EventDispatch;");
    jmethodID keyTypedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "keyReleased", "(Ljava/lang/String;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, keyTypedMethod, toJstring(key));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void clickMouse_(int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/EventDispatch;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "mouseClicked", "(II)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, toJint(x), toJint(y));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void pressMouse_(int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/EventDispatch;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "mousePressed", "(II)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, toJint(x), toJint(y));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void dragMouse_(int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/EventDispatch;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "mouseDragged", "(II)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, toJint(x), toJint(y));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void releaseMouse_(int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/EventDispatch;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "mouseReleased", "(II)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, toJint(x), toJint(y));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void moveMouse_(int x, int y, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/EventDispatch;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "mouseMoved", "(II)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, toJint(x), toJint(y));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void setRandom_(int seed, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/Random;");
    jmethodID setMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "set", "(I)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, setMethod, toJint(seed));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

void setAsk_(const char* nextAsk, const char* _file, const char* _function, int _line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lgreenfoot/junitUtils/Ask;");
    jmethodID keyTypedMethod = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "setAsk", "(Ljava/lang/String;)V");
	(*javaEnv)->CallStaticVoidMethod(javaEnv, cls, keyTypedMethod, toJstring(nextAsk));
    if ((*javaEnv)->ExceptionOccurred(javaEnv)) {
    	throwAssertionError("Must be used in a test", _file, _function, _line);
    }
}

#endif
