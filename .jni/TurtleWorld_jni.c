/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"

#include "greenfoot.h"
extern JNIEnv *javaEnv;

#include "TurtleWorld.c"

JNIEXPORT void JNICALL Java_TurtleWorld_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startTurtleWorld(object);
}

JNIEXPORT jobject JNICALL Java_TurtleWorld_getTurtle_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    return getTurtle(object);
}

JNIEXPORT jint JNICALL Java_TurtleWorld_width
  (JNIEnv *env, jclass clazz)
{
#ifdef _width
	return toJint(_width);
#else
	return toJint(400);
#endif
}

JNIEXPORT jint JNICALL Java_TurtleWorld_height
  (JNIEnv *env, jclass clazz)
{
#ifdef _height
	return toJint(_height);
#else
	return toJint(200);
#endif
}

JNIEXPORT jint JNICALL Java_TurtleWorld_cellSize
  (JNIEnv *env, jclass clazz)
{
#ifdef _cellSize
	return toJint(_cellSize);
#else
	return toJint(1);
#endif
}
