import greenfoot.*;

/**
 * The TurtleWorld class defines a TurtleWorld of 11 cells on the X axis by 5 cells 
 * on the Y axis, where each cell is drawn using a 60x60 size image 
 * built from the "sand.jpg" file. When a scenario is executed, Greenfoot 
 * executes the act() method of all the actors repeatedly, that is, the 
 * execution of a Greenfoot scenario is a sequence of cycles and in each 
 * cycle the act () method of all the objects it contains is executed 
 * scenario.
 * 
 * @author (Francisco Guerra) 
 * @version (Version 1)
 */
public class TurtleWorld extends World {

	public TurtleWorld() {
    	super(width(), height(), cellSize());
		start();
	}
    
    public void start(){
        start_();
    }
    private native void start_();

    public Turtle getTurtle() {
        return getTurtle_();
    }
    private native Turtle getTurtle_();

    private native static int width();
    private native static int height();
    private native static int cellSize();
    
    static {
        System.load(new java.io.File(".jni", "TurtleWorld_jni.so").getAbsolutePath());
    }
}
