/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "Turtle.c"

#include "greenfoot.h"
extern JNIEnv  *javaEnv;

JNIEXPORT void JNICALL Java_Turtle_start_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    startTurtle(object);
}

JNIEXPORT jobject JNICALL Java_Turtle_act_1
  (JNIEnv *env, jobject object)
{
	javaEnv = env;
    actTurtle(object);
}

JNIEXPORT void JNICALL Java_Turtle_setAngle_1
  (JNIEnv *env, jobject object, jint angle)
{
	javaEnv = env;
    setAngle(object, toInt(angle));
}

JNIEXPORT void JNICALL Java_Turtle_setDistance_1
  (JNIEnv *env, jobject object, jint distance)
{
	javaEnv = env;
    setDistance(object, toInt(distance));
}
